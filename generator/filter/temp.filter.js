/*
* <%= upCaseName %> filter file
*/

/** @ngInject */
const <%= name %>Filter = function <%= name %>FilterFunction(/* dependency injection goes here */) {
  
    /*
    * filter code goes here
    */

    return {
        /*
        * return filter returned object
        */
    }
};

export default <%= name %>Filter;