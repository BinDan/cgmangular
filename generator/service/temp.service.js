/*
* <%= upCaseName %> service file
*/

/** @ngInject */
const <%= name %>Service = function <%= name %>ServiceFunction(/* dependency injection goes here */) {
  
    /*
    * service code goes here
    */

    return {
        /*
        * return service returned object
        */
    }
};

export default <%= name %>Service;
