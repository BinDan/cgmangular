/*
* <%= upCaseName %> Controller file
* @class
*/
class <%= upCaseName %>Controller {
    /** @ngInject */
    constructor(/* dependency injection goes here */) {
        this.name = '<%= name %>';

        /*
        * component controller code goes here
        */
    }
}

export default <%= upCaseName %>Controller;
