import angular from 'angular';
import uiRouter from 'angular-ui-router';
import <%= name %>Component from './<%= name %>.component';

const <%= name %>Module = angular.module('<%= name %>', [
    uiRouter
])

    .component('<%= name %>', <%= name %>Component)

    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('<%= parentState %><%= name %>', {
                url: '/<%= decName %>',
                template: '<<%= decName %>></<%= decName %>>'
            });
    })

    .name;

export default <%= name %>Module;
