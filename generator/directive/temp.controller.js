/*
* <%= upCaseName %> directive file
* @class
*/
class <%= upCaseName %>Controller {
    /** @ngInject */
    constructor(/* dependency injection goes here */) {
        this.name = '<%= name %>';

        /*
        * directive controller code goes here
        */
    }
}

export default <%= upCaseName %>Controller;
