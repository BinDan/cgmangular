import template from './<%= name %>.html';
import controller from './<%= name %>.controller';

/** @ngInject */
const <%= name %>Directive = function <%= name %>DirectiveFunction(/* dependency injection goes here */) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {},
        link: function (scope, element, attrs) {
            /*
            * directive link function code goes here
            * if link function code is too long, use import as for the controller
            */
        },
        template,
        controller,
        controllerAs: '<%= name %>'
    };
};

export default <%= name %>Directive;
