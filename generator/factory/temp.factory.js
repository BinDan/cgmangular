/*
* <%= upCaseName %> factory file
*/

/** @ngInject */
const <%= name %>Factory = function <%= name %>FactoryFunction(/* dependency injection goes here */) {
  
    /*
    * factory code goes here
    */

    return {
        /*
        * return factory returned object
        */
    }
};

export default <%= name %>Factory;
