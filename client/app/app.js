import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import ngResource from 'angular-resource';
import animate from 'angular-animate';
import ngFileUpload from 'ng-file-upload';
import rzModule from 'angularjs-slider';
import ngScroll from 'angular-scroll';
import 'bootstrap-no-fonts-no-js'; // eslint-disable-line import/extensions
import './assets/less/main.less';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import Restangular from 'restangular';


require('./../../node_modules/angular-i18n/angular-locale_it.js');

const app = angular.module('app', [
    uiRouter,
    uiBootstrap,
    ngResource,
    animate,
    ngFileUpload,
    rzModule,
    ngScroll,
    Common,
    Components,
    Restangular
])

  .config(($locationProvider,CONFIG) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  })

    .component('app', AppComponent);

function fetchData() {
    const initInjector = angular.injector(['ng']);
    const $http = initInjector.get('$http');
    const $log = initInjector.get('$log');

    return $http.get('./config.json').then((response) => {
        app.constant('CONFIG', response.data);

//console.log(CONFIG);

    }, (error) => {
        $log.error(error);
    });
}

function bootstrapApplication() {
    angular.element(document).ready(() => {     // eslint-disable-line no-undef
        angular.bootstrap(document, ['app']);   // eslint-disable-line no-undef
    });
}

fetchData().then(bootstrapApplication);
