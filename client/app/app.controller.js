/*
 * App Controller file
 * @class
 */
class AppController {
    /** @ngInject */
    constructor(CONFIG,httpHeaders,Restangular) {

    let headers = {};

    if(httpHeaders.getHeaders()){
        angular.extend(headers, {'X-Auth-Token': httpHeaders.getHeaders()});
    }

    Restangular.setDefaultHeaders(headers);    	

    } // eslint-disable-line
}

export default AppController;
