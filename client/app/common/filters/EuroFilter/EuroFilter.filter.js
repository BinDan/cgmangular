/*
* EuroFilter filter file
*/

/** @ngInject */
const EuroFilter = function EuroFilterFunction($sce) {
    return (value, precision, strongInt, withCurrency, hideZeros, returnOnlyValue) => {
        let parsedValue;
        let returnValue;
        const decimalPrecision = parseInt(precision || 2, 10);

        if (typeof value === 'string') {
            parsedValue = parseFloat(value.replace(',', '.')).toFixed(decimalPrecision);
        }

        if (typeof value === 'string' && value === '-') {
            return $sce.trustAsHtml('-');
        }

        if (typeof value === 'number') {
            parsedValue = parseFloat(value).toFixed(decimalPrecision);
        }

        if (!value) {
            parsedValue = parseFloat(0).toFixed(decimalPrecision);
        }

        if (!isNaN(parsedValue)) {
            let valueStr = parsedValue.toString();
            const valueStrDec = valueStr.slice(-decimalPrecision);
            const valueFormat = [];

            valueStr = valueStr.substring(0, valueStr.length - (decimalPrecision + 1));

            while (valueStr.length > 3) {
                valueFormat.unshift(valueStr.slice(-3));
                valueStr = valueStr.substring(0, valueStr.length - 3);
            }

            valueFormat.unshift(valueStr);

            if (valueFormat[0] !== '-') {
                returnValue = `${valueFormat.join('.')},${valueStrDec}`;

                if (strongInt) {
                    if (hideZeros && parseInt(valueStrDec, 10) === 0) {
                        returnValue = `<strong>${valueFormat.join('.')}`;
                    } else {
                        returnValue = `<strong>${valueFormat.join('.')},</strong>${valueStrDec}`;
                    }
                }

                if (withCurrency) {
                    returnValue = `${returnValue} €`;
                }
            }
        } else {
            returnValue = '';
        }

        if (!returnOnlyValue) {
            return $sce.trustAsHtml(returnValue);
        }

        return returnValue;
    };
};

export default EuroFilter;
