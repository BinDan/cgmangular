/*
 * CapitalizeFilter filter file
 */

/** @ngInject */
const CapitalizeFilter = function CapitalizeFilterFunction() {
    return (input) => {
        if (input !== null) {
            return input.replace(/\w\S*/g, (txt) => {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
        return '';
    };
};

export default CapitalizeFilter;
