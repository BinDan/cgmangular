/*
* ConvertDate filter file
*/

/** @ngInject */
const convertDateFilter = function convertDateFilterFunction($filter) {

var angularDateFilter = $filter('date');
  
return (dateStr,formatStr) => {
        var res = dateStr;
        
        var format = (formatStr ? formatStr : "dd/MM/yyyy");
        
        if (res && isNaN(res)) {
            var pattern = new RegExp("^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])(\s(2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9])*");
            
            if (pattern.test(res)) {
                var d = new Date(Date.parse(res));
                
                if (Object.prototype.toString.call(d) === "[object Date]") {
                    // it is a date
                    if (!isNaN(d.getTime())) {  // d.valueOf() could also work                       
                        res = angularDateFilter(d, format);
                    }
                }
            }
        }
        
        return res;
    };
};

export default convertDateFilter;