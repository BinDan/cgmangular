import angular from 'angular';
/* gulp.import */
import convertDate from './convertDate/convertDate.filter';
import EuroFilter from './EuroFilter/EuroFilter.filter';
import CapitalizeFilter from './CapitalizeFilter/CapitalizeFilter.filter';

const filterModule = angular.module('filter', [])
/* gulp.inject */
    .filter('convertDate', convertDate)
    .filter('capitalize', CapitalizeFilter)
    .filter('EuroFilter', EuroFilter);

export default filterModule;
