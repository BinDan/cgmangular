import angular from 'angular';
/* gulp.import */

const factoryModule = angular.module('factory', [])
/* gulp.inject */
;

export default factoryModule;
