import angular from 'angular';
import Services from './services/service';
import Factories from './factories/factory';
import Directives from './directives/directive';
import Filters from './filters/filter';

const commonModule = angular.module('app.common', [
    Services.name,
    Factories.name,
    Directives.name,
    Filters.name
])

    .name;

export default commonModule;
