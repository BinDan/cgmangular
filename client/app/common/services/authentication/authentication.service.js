/*
* Authentication service file
*/

/** @ngInject */
const authenticationService = function authenticationServiceFunction($q,$http,cgmStorage,CONFIG,$state, $window) {

    let _self = this;
  
    let LOCAL_TOKEN_KEY = 'token';
    this.username = '';
    this.userid = '';
    this.isAuthenticated = false;
    this.role = '';
    this.token = '';
    this.authToken;
    this.persontype



    this.loadUserCredentials = () => {
        this.token = cgmStorage.getLocal(LOCAL_TOKEN_KEY);
        this.username = cgmStorage.getLocal('username');
        this.userid = cgmStorage.getLocal('userid');
        var persontype = cgmStorage.getLocal('persontype');
        var persontypeid = cgmStorage.getLocal('persontypeid');
        var issuperuser = cgmStorage.getLocal('issuperuser');

        if (this.token) {
            this.useCredentials(persontype, persontypeid, issuperuser,this.token);
        }
    }


    this.useCredentials = (persontype, persontypeid, issuperuser,token) => {
	    this.isAuthenticated = true;
	    this.authToken = token;

	    //role = USER_ROLES.public;

	    if (persontype) {
	        var persontype = persontype.toLowerCase();
	        switch (persontype) {
	        case 'amministratore':
	//                    role = USER_ROLES.customer;
	            this.role = 'amministratore_role';
	            break;

	        case 'owner':
	            this.role = 'owner_role';
	            break;
	        }
	    }

	    if (issuperuser == '1') {
	        this.role = 'superuser_role';
	    }

//            // Set the token as header for your requests!
//            $http.defaults.headers.common['X-Auth-Token'] = this.authToken + '|Desktop';
    }

    this.storeUserCredentials = (val_userid, val_username, persontype, persontypeid, issuperuser, token) => {

        _self.userid = val_userid;
        _self.username = val_username;
//        permissions  = JSON.stringify(perm);

        cgmStorage.setLocal('userid', val_userid);
        cgmStorage.setLocal(LOCAL_TOKEN_KEY, token);
        cgmStorage.setLocal('username', val_username);
        cgmStorage.setLocal('persontype', persontype);
        cgmStorage.setLocal('persontypeid', persontypeid);
        cgmStorage.setLocal('issuperuser', issuperuser);


        /*if (permissions.length > 0) {
            console.log(permissions);
        }*/
    //    window.localStorage.setItem('permissions', permissions);

        this.useCredentials(val_username, persontype, persontypeid, issuperuser, token);
    }


        this.destroyUserCredentials = () => {
            this.authToken = undefined;
            this.username = '';
            this.isAuthenticated = false;
//            $http.defaults.headers.common['X-Auth-Token'] = undefined;

            cgmStorage.removeLocal(LOCAL_TOKEN_KEY);
            cgmStorage.removeLocal('username');
            cgmStorage.removeLocal('permissions');
        }


        this.logout = () => {
            this.destroyUserCredentials();
            $state.go('home');
        };


        this.login = (name, pw) => {
            return $q( (resolve, reject) => {
                $http({
                    url: CONFIG.rest.BASE_URL + CONFIG.rest.getLogin.url,
                    method: 'POST',
                    data: "email=" + name + "&password=" + pw,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function successCallback(response) {

// console.log(response);  

                    if (response.data.token) {

// console.log(response.data);

                        _self.storeUserCredentials(response.data.id, response.data.name + ' ' + response.data.lastname, response.data.persontype.replace(' ',''),
                            response.data.idpersontype, response.data.issuperuser, response.data.token);

                        $window.location = $state.get('dashboard');
                        
                        return true;
                    }
                    alert('Login failed! Please check your credentials!');
                    //sweetAlert("Oops...", "Login failed! Please check your credentials!", "error");
                }, function errorCallback(response) {
                    alert('Login failed! Please check your credentials!');
                    //sweetAlert("Oops...", "Login failed! Please check your credentials!", "error");
                    //reject('Login Failed.');
                });

                /*if ((name == 'admin' && pw == '1') || (name == 'user' && pw == '1')) {
                  // Make a request and receive your auth token from your server
                  storeUserCredentials(name + '.yourServerToken');
                  resolve('Login success.');
                } else {
                  reject('Login Failed.');
                }*/
            });
        };


	this.getToken = () => {
        return this.authToken;
    }


    this.loadUserCredentials();


};

export default authenticationService;
