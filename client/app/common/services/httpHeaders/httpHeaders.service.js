/*
* HttpHeaders service file
*/

/** @ngInject */
const httpHeadersService = function httpHeadersServiceFunction(authentication, Restangular) {
  
    this.getHeaders = () =>  {
        if(authentication.getToken()){
        	return authentication.getToken() + "|Desktop";
        } else {
        	return null;
        }
        
    };

};

export default httpHeadersService;
