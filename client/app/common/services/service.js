import angular from 'angular';
/* gulp.import */
import httpHeaders from './httpHeaders/httpHeaders.service';
import cgmStorage from './cgmStorage/cgmStorage.service';
import authentication from './authentication/authentication.service';

const serviceModule = angular.module('service', [])
/* gulp.inject */
    .service('httpHeaders', httpHeaders)
    .service('cgmStorage', cgmStorage)
    .service('authentication', authentication)

;

export default serviceModule;
