/*
* CgmStorage service file
*/

/** @ngInject */
const cgmStorageService = function cgmStorageServiceFunction($window) {
  
    /* session */
    this.setSession = (key, value) => {
        $window.sessionStorage.setItem(key, JSON.stringify(value));
    };

    this.getSession = (key) => {
        if ($window.sessionStorage[key]) {
            return JSON.parse($window.sessionStorage[key]);
        }

        return undefined;
    };

    this.removeSession = (key) => {
        if ($window.sessionStorage[key]) {
            $window.sessionStorage.removeItem(key);
        }
    };

    this.clearSession = () => {
        $window.sessionStorage.clear();
    };

    /* local */
    this.setLocal = (key, value) => {
        $window.localStorage.setItem(key, JSON.stringify(value));
    };

    this.getLocal = (key) => {
        if ($window.localStorage[key]) {
            return JSON.parse($window.localStorage[key]);
        }

        return undefined;
    };

    this.removeLocal = (key) => {
        if ($window.localStorage[key]) {
            $window.localStorage.removeItem(key);
        }
    };

    this.clearLocal = () => {
        $window.localStorage.clear();
    };
};

export default cgmStorageService;
