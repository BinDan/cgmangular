/*
* CgmAlert directive file
* @class
*/
class CgmAlertController {
    /** @ngInject */
    constructor(cgmAlertManager) {
        this.name = 'cgmAlert';
        this.close = (index) => {
            cgmAlertManager.closeAlert(index);
        };
    }
}

export default CgmAlertController;
