import template from './cgmAlert.html';
import controller from './cgmAlert.controller';

/** @ngInject */
const cgmAlertDirective = function cgmAlertDirectiveFunction(/* dependency injection goes here */) {
    return {
        restrict: 'AE',
        template,
        controller,
        controllerAs: 'cgmAlert'
    };
};

export default cgmAlertDirective;
