/*
* CgmHeader directive file
* @class
*/
class CgmHeaderController {
    /** @ngInject */
    constructor(/* dependency injection goes here */) {
        this.name = 'cgmHeader';

        /*
        * directive controller code goes here
        */
    }
}

export default CgmHeaderController;
