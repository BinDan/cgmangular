import template from './cgmHeader.html';
import controller from './cgmHeader.controller';

/** @ngInject */
const cgmHeaderDirective = function cgmHeaderDirectiveFunction(/* dependency injection goes here */) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {},
        link: function (scope, element, attrs) {
            /*
            * directive link function code goes here
            * if link function code is too long, use import as for the controller
            */
        },
        template,
        controller,
        controllerAs: 'cgmHeader'
    };
};

export default cgmHeaderDirective;
