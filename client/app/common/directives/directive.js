import angular from 'angular';
/* gulp.import */
import cgmAlert from './cgmAlert/cgmAlert.directive';
import cgmHeader from './cgmHeader/cgmHeader.directive';

const directiveModule = angular.module('directive', [])
/* gulp.inject */
    .directive('cgmAlert', cgmAlert)
    .directive('cgmHeader', cgmHeader)
; 

export default directiveModule;
