import angular from 'angular';
import uiRouter from 'angular-ui-router';
import logoutComponent from './logout.component';

const logoutModule = angular.module('logout', [
    uiRouter
])

    .component('logout', logoutComponent)

    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('logout', {
                url: '/logout',
                template: '<logout></logout>'
            });
    })

    .name;

export default logoutModule;
