/*
* Logout Controller file
* @class
*/
class LogoutController {
    /** @ngInject */
    constructor(authentication) {
        this.name = 'logout';

        this.userLogout = () => {
        	authentication.logout();
        }
    }
}

export default LogoutController;
