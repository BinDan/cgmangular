import template from './logout.html';
import controller from './logout.controller';

/*
* logout Component
*/
const logoutComponent = {
    restrict: 'E',
    bindings: { },
    template,
    controller
};

export default logoutComponent;
