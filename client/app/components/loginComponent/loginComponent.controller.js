/*
* LoginComponent Controller file
* @class
*/
class LoginComponentController {
    /** @ngInject */
    constructor(authentication,$scope) {
        this.name = 'loginComponent';

        /*
        * component controller code goes here
        */

        this.userLogin = () => {
        	authentication.login($scope.email,$scope.password);
        }

    }
}

export default LoginComponentController;
