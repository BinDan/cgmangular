import angular from 'angular';
import uiRouter from 'angular-ui-router';
import loginComponentComponent from './loginComponent.component';

const loginComponentModule = angular.module('loginComponent', [
    uiRouter
])

    .component('loginComponent', loginComponentComponent)

    .config(($stateProvider, $urlRouterProvider) => {
        'ngInject';

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('loginComponent', {
                url: '/login-component',
                template: '<login-component></login-component>'
            });
    })

    .name;

export default loginComponentModule;
