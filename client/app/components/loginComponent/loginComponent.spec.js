import LoginComponentModule from './loginComponent'
import LoginComponentController from './loginComponent.controller';
import LoginComponentComponent from './loginComponent.component';
import LoginComponentTemplate from './loginComponent.html';

describe('LoginComponent', () => {
    let $rootScope, makeController;

    beforeEach(window.module(LoginComponentModule));
    beforeEach(inject((_$rootScope_) => {
        $rootScope = _$rootScope_;
        makeController = () => {
            return new LoginComponentController();
        };
    }));

    describe('Module', () => {
        // top-level specs: i.e., routes, injection, naming
    });

    describe('Controller', () => {
        // controller specs
        it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
            let controller = makeController();
            expect(controller).to.have.property('name');
        });
    });

    describe('Template', () => {
        // template specs
        // tip: use regex to ensure correct bindings are used e.g., {{  }}
        it('has name in template [REMOVE]', () => {
            expect(LoginComponentTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
        });
    });

    describe('Component', () => {
        // component/directive specs
        let component = LoginComponentComponent;

        it('includes the intended template', () => {
            expect(component.template).to.equal(LoginComponentTemplate);
        });

        it('invokes the right controller', () => {
            expect(component.controller).to.equal(LoginComponentController);
        });
    });
});
