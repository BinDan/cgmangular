import template from './loginComponent.html';
import controller from './loginComponent.controller';

/*
* loginComponent Component
*/
const loginComponentComponent = {
    restrict: 'E',
    bindings: { },
    template,
    controller
};

export default loginComponentComponent;
