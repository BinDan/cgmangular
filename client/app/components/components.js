import angular from 'angular';
/* gulp.import */
import Logout from './logout/logout';
import LoginComponent from './loginComponent/loginComponent';

const componentModule = angular.module('app.components', [
/* gulp.inject */
    Logout,
    LoginComponent,

]).name;

export default componentModule;
