== Guida all'installazione ==

1.  Clonare il repository del progetto tramite SourceTree o tool GIT affini.
2.  Posizionarsi nella cartella del progetto appena scaricato.
3.  Aprire un terminare con privilegi di Amministratore.
4.  Eseguire i comandi riportati di seguito:


    - Solo la prima volta: 

    `npm install -g gulp karma karma-cli webpack`


    - Ogni volta che si clona il progetto: 

    `npm install`


- Per eseguire il progetto in modalità di sviluppo tramite server locale, usare il comando: 

    `gulp`


- Per creare i file da distribuire, usare il comando: 

    `gulp webpack`


- Nel terminale viene indicato l'indirizzo locale su cui vedere il progetto, ad esempio:

    `[BS] Access URLs:`

    `Local: http://localhost:3000`

    `External: http://127.00.00.186:3000`


- Per creare nuovi componenti, direttive, factory e servizi ultilizzare nel terminale i seguenti comandi:

    `gulp new-component --name [componentName]`

    `gulp new-directive --name [directiveName]`

    `gulp new-factory --name [factoryName]`

    `gulp new-service --name [serviceName]`


Usando questi comandi, vengono automaticamente generati i file necessari secondo la seguente struttura:
- per i **componenti**, nel path //client/app/components///:


    componentName/

    ....componentName.js            // entry file

    ....componentName.component.js  // definzione componente

    ....componentName.controller.js // logica del componente

    ....componentName.html          // template

    ....componentName.spec.js       // test karma


    app/assets/less/components/

    ....componentName.less          // stile


- per le **direttive**, nel path //client/app/common/directives//:


    directivetName/

    ....directivetName.directive.js  // definzione direttiva

    ....directivetName.controller.js // logica della direttiva

    ....directivetName.html          // template


    app/assets/less/directives/

    ....directivetName.less          // stile


- per le **factory**, nel path //client/app/common/factories//:


    factoryName/

        ....actoryName.factory.js      // definzione factory


- per i **servizi**, nel path //client/app/common/services//:


    serviceName/

    ....serviceName.factory.js      // definzione servizio


- E' possibile creare i file in una cartella diversa da quella di default, indicata sopra, tramite il parametro //--parent//:

    `gulp component --name [componentName] --parent [parentComponentName]`