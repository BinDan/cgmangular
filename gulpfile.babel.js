'use strict'; // eslint-disable-line

import gulp from 'gulp';
import webpack from 'webpack';
import path from 'path';
import rename from 'gulp-rename';
import template from 'gulp-template';
import yargs from 'yargs';
import gutil from 'gulp-util';
import serve from 'browser-sync';
import replace from 'gulp-replace';
import del from 'del';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import colorsSupported from 'supports-color';
import historyApiFallback from 'connect-history-api-fallback';
import decamelize from 'decamelize';
import cheerio from 'gulp-cheerio';
import moment from 'moment';

const root = 'client';

// helper method for resolving paths
const resolveToApp = (glob = '') => {
    return path.join(root, 'app', glob); // app/{glob}
};

const resolveToComponents = (glob = '') => {
    return path.join(root, 'app/components', glob); // app/components/{glob}
};

const resolveToComponentsLess = (glob = '') => {
    return path.join(root, 'app/assets/less/components', glob); // app/assets/less/components/{glob}
};

const resolveToDirectives = (glob = '') => {
    return path.join(root, 'app/common/directives', glob); // app/common/directives/{glob}
};

const resolveToDirectivesLess = (glob = '') => {
    return path.join(root, 'app/assets/less/directives', glob); // app/assets/less/directives/{glob}
};

const resolveToServices = (glob = '') => {
    return path.join(root, 'app/common/services', glob); // app/common/services/{glob}
};

const resolveToFactories = (glob = '') => {
    return path.join(root, 'app/common/factories', glob); // app/common/factories/{glob}
};

const resolveToFilters = (glob = '') => {
    return path.join(root, 'app/common/filters', glob); // app/common/factories/{glob}
};

// map of all paths
const paths = {
    js: resolveToComponents('**/*!(.spec.js).js'), // exclude spec files
    less: resolveToApp('**/*.less'), // stylesheets
    html: [
        resolveToApp('**/*.html'),
        path.join(root, 'index.html')
    ],
    entry: [
        'babel-polyfill',
        path.join(__dirname, root, 'app/app.js')
    ],
    output: root,
    blankTemplates: path.join(__dirname, 'generator', 'component/**/*.**'),
    blankDirectiveTemplates: path.join(__dirname, 'generator', 'directive/**/*.**'),
    blankServiceTemplates: path.join(__dirname, 'generator', 'service/**/*.**'),
    blankFactoryTemplates: path.join(__dirname, 'generator', 'factory/**/*.**'),
    blankFilterTemplates: path.join(__dirname, 'generator', 'filter/**/*.**'),
    dest: path.join(__dirname, 'dist')
};

let temp;
let parentTemp;

const date = moment().format('DD/MM/YY');

gulp.task('version', () => {
    return gulp.src(path.join(root, 'app/common/directives/ispbTooltip/ispbTooltip.html', ''))
        .pipe(cheerio({
            run: ($) => {
                // Each file will be run through cheerio and each corresponding `$` will be passed here.
                const versionCode = yargs.argv.version || '0.x';
                $('#version-code').html(`ver. ${versionCode}<br>${date}`);
            },
            parserOptions: {
                xmlMode: true,
                decodeEntities: false
            }
        })).pipe(gulp.dest(path.join(root, 'app/common/directives/ispbTooltip')));
});

// use webpack.config.js to build modules
gulp.task('webpack', ['version', 'clean'], (cb) => {
    const config = require('./webpack.dist.config'); // eslint-disable-line global-require

    config.entry.app = paths.entry;

    webpack(config, (err, stats) => {
        if (err) {
            throw new gutil.PluginError('webpack', err);
        }

        gutil.log('[webpack]', stats.toString({
            colors: colorsSupported,
            chunks: false,
            errorDetails: true
        }));

        cb();
    });

    gulp.start('copy-config.json');
    gulp.start('copy-favicon.ico');
});

gulp.task('serve', () => {
    const config = require('./webpack.dev.config'); // eslint-disable-line global-require

    config.entry.app = [
        // this modules required to make HRM working
        // it responsible for all this webpack magic
        'webpack-hot-middleware/client?reload=true',
        // application entry point
    ].concat(paths.entry);

    const compiler = webpack(config); // eslint-disable-line vars-on-top

    serve({
        port: process.env.PORT || 3000,
        open: false,
        ghostMode: false,
        server: {
            baseDir: root
        },
        middleware: [
            historyApiFallback(),
            webpackDevMiddleware(compiler, {
                stats: {
                    colors: colorsSupported,
                    chunks: false,
                    modules: false
                },
                publicPath: config.output.publicPath
            }),
            webpackHotMiddleware(compiler)
        ]
    });
});

gulp.task('watch', ['serve']);

// generator of components
gulp.task('component', () => {
    const cap = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1);
    };
    const checkParent = (val) => {
        return val !== '' ? val.concat('.') : '';
    };
    const name = yargs.argv.name;
    const parentPath = yargs.argv.parent || '';
    const destPath = path.join(resolveToComponents(), parentPath, name);

    temp = name;
    parentTemp = parentPath;

    return gulp.src(paths.blankTemplates)
        .pipe(template({
            name: name,
            upCaseName: cap(name),
            decName: decamelize(name, '-'),
            parentState: checkParent(parentPath)
        }))
        .pipe(rename((path) => { // eslint-disable-line no-shadow
            path.basename = path.basename.replace('temp', name); // eslint-disable-line no-param-reassign
        }))
        .pipe(gulp.dest(destPath));
});

gulp.task('new-component', ['component'], () => {
    let src;
    let importPath;

    if (parentTemp.length > 0) {
        src = resolveToComponents(`\\${parentTemp}\\${temp}\\${temp}.less`);
        importPath = `'./${parentTemp}/${temp}/${temp}'`;
    } else {
        src = resolveToComponents(`\\${temp}\\${temp}.less`);
        importPath = `'./${temp}/${temp}'`;
    }

    const dest = resolveToComponentsLess();

    gulp.src(src)
        .pipe(gulp.dest(dest));

    del(src);

    gulp.src(resolveToComponents().concat('\\components.js'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\nimport ${temp.charAt(0).toUpperCase() + temp.slice(1)} from ${importPath};`)))
        .pipe(replace('/* gulp.inject */', '/* gulp.inject */'.concat(`\n    ${temp.charAt(0).toUpperCase() + temp.slice(1)},`)))
        .pipe(gulp.dest(resolveToComponents()));

    gulp.src(resolveToComponentsLess('components.less'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\n@import '${temp}.less';`)))
        .pipe(gulp.dest(dest));
});

// generator of directives
gulp.task('directive', () => {
    const cap = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1);
    };
    const name = yargs.argv.name;
    const parentPath = yargs.argv.parent || '';
    const destPath = path.join(resolveToDirectives(), parentPath, name);

    temp = name;

    return gulp.src(paths.blankDirectiveTemplates)
        .pipe(template({
            name: name,
            upCaseName: cap(name),
            decName: decamelize(name, '-')
        }))
        .pipe(rename((path) => { // eslint-disable-line no-shadow
            path.basename = path.basename.replace('temp', name); // eslint-disable-line no-param-reassign
        }))
        .pipe(gulp.dest(destPath));
});

gulp.task('new-directive', ['directive'], () => {
    const src = resolveToDirectives(`\\${temp}\\${temp}.less`);
    const dest = resolveToDirectivesLess();

    gulp.src(src)
        .pipe(gulp.dest(dest));

    del(src);

    gulp.src(resolveToDirectives().concat('\\directive.js'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\nimport ${temp} from './${temp}/${temp}.directive';`)))
        .pipe(replace('/* gulp.inject */', '/* gulp.inject */'.concat(`\n    .directive('${temp}', ${temp})`)))
        .pipe(gulp.dest(resolveToDirectives()));

    gulp.src(resolveToDirectivesLess('directives.less'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\n@import '${temp}.less';`)))
        .pipe(gulp.dest(dest));
});

// generator of services
gulp.task('service', () => {
    const cap = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1);
    };
    const name = yargs.argv.name;
    const parentPath = yargs.argv.parent || '';
    const destPath = path.join(resolveToServices(), parentPath, name);

    temp = name;

    return gulp.src(paths.blankServiceTemplates)
        .pipe(template({
            name: name,
            upCaseName: cap(name)
        }))
        .pipe(rename((path) => { // eslint-disable-line no-shadow
            path.basename = path.basename.replace('temp', name); // eslint-disable-line no-param-reassign
        }))
        .pipe(gulp.dest(destPath));
});

gulp.task('new-service', ['service'], () => {
    gulp.src(resolveToServices().concat('\\service.js'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\nimport ${temp} from './${temp}/${temp}.service';`)))
        .pipe(replace('/* gulp.inject */', '/* gulp.inject */'.concat(`\n    .service('${temp}', ${temp})`)))
        .pipe(gulp.dest(resolveToServices()));
});

// generator of factory
gulp.task('factory', () => {
    const cap = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1);
    };
    const name = yargs.argv.name;
    const parentPath = yargs.argv.parent || '';
    const destPath = path.join(resolveToFactories(), parentPath, name);

    temp = name;

    return gulp.src(paths.blankFactoryTemplates)
        .pipe(template({
            name: name,
            upCaseName: cap(name)
        }))
        .pipe(rename((path) => { // eslint-disable-line no-shadow
            path.basename = path.basename.replace('temp', name); // eslint-disable-line no-param-reassign
        }))
        .pipe(gulp.dest(destPath));
});

gulp.task('new-factory', ['factory'], () => {
    gulp.src(resolveToFactories().concat('\\factory.js'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\nimport ${temp} from './${temp}/${temp}.factory';`)))
        .pipe(replace('/* gulp.inject */', '/* gulp.inject */'.concat(`\n    .factory('${temp}', ${temp})`)))
        .pipe(gulp.dest(resolveToFactories()));
});


// generator of factory
gulp.task('filter', () => {
    const cap = (val) => {
        return val.charAt(0).toUpperCase() + val.slice(1);
    };
    const name = yargs.argv.name;
    const parentPath = yargs.argv.parent || '';
    const destPath = path.join(resolveToFilters(), parentPath, name);

    temp = name;

    return gulp.src(paths.blankFilterTemplates)
        .pipe(template({
            name: name,
            upCaseName: cap(name)
        }))
        .pipe(rename((path) => { // eslint-disable-line no-shadow
            path.basename = path.basename.replace('temp', name); // eslint-disable-line no-param-reassign
        }))
        .pipe(gulp.dest(destPath));
});

gulp.task('new-filter', ['filter'], () => {
    gulp.src(resolveToFilters().concat('\\filter.js'))
        .pipe(replace('/* gulp.import */', '/* gulp.import */'.concat(`\nimport ${temp} from './${temp}/${temp}.filter';`)))
        .pipe(replace('/* gulp.inject */', '/* gulp.inject */'.concat(`\n    .filter('${temp}', ${temp})`)))
        .pipe(gulp.dest(resolveToFilters()));
});




gulp.task('clean', (cb) => {
    del([paths.dest]).then((paths) => { // eslint-disable-line func-names, no-shadow
        gutil.log('[clean]', paths);
        cb();
    });
});

gulp.task('copy-config.json', () => {
    gulp.src('./client/config.json').pipe(gulp.dest('./dist'));
});

gulp.task('copy-favicon.ico', () => {
    gulp.src('./client/favicon.ico').pipe(gulp.dest('./dist'));
});

gulp.task('default', ['watch']);
